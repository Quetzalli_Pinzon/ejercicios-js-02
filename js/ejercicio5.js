// Dado el siguiente arreglo genera la función para imprimir en consola el num max y el num min

//var myArray = [1, 2, 3, 4]
var myArray = [1, -42, 9, 0]


// function minMax(){
//     maxNum = 0;
//     minNum = 0;
//     console.log("exito")
//     for(let i = 0; i < myArray.length; i++){
//         if(myArray[i] > maxNum){
//         maxNum = myArray[i];
//         }
//     }
//     console.log("El numero máximo es: " + maxNum)
// }

function minMax(){
    console.log("El número máximo es: " + Math.max(...myArray));
    console.log("El número mínimo es: " + Math.min(...myArray));
}

minMax(myArray);