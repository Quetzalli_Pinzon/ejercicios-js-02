var clientes = ['María Sánchez', 'Guadalupe Martínez'];
var empleados = ['Josefina López', 'Martha Ramirez'];

console.log(clientes.concat(empleados)); // La opción adecuada
// Se crea un array con todos los nombres de ambos arrays

console.log(clientes.join(empleados));
// Concatena todos los elementos del array en un único String, separa los datos de cada array con ,

console.log(clientes.push(empleados));
// devuelve el tamaño del array

console.log(clientes.splice(empleados));
// Agrega un nuevo array al array principal

