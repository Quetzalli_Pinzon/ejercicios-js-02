// Teniendo el siguiente objeto genera una función para pasar cada elemento a un arreglo y ordenarlos segun su valor

var object = {
    key1: 10,
    key2: 3,
    key3: 40,
    key4: 20,
    // key5: 0,
    // key6: 2,
    // key7: -96
}

function toArray(){
    array = Object.values(object); // convierto el objeto en array
    //console.log(array)
    order = array.sort(function(a, b){  // funcion de comparación para ordenar los elementos del array
        return a - b;
    });
    console.log(order)
}

toArray(object);